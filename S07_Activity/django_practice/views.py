from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import GroceryItem


def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {'groceryitem_list': groceryitem_list}
	return render(request, "django_practice/index.html",context)

def groceryitem(request,groceryitem_id):
	response = "You are viewing grocery item no. %s"
	return HttpResponse(response % groceryitem_id)